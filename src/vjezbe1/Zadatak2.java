package vjezbe1;

import java.util.Scanner;

class Zadatak2 {

    private double[] novcanice = {1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.20, 0.10, 0.05, 0.02, 0.01};
    private int index = 0;

    public Zadatak2() {
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        double iznos = Double.parseDouble(line);
        izvratiOstatak(iznos);
    }

    private void izvratiOstatak(double iznos) {
        iznos = zaokruziCudniDouble(iznos, 2);
        int brojac = 0;
        while (iznos - novcanice[index] >= 0) {
            iznos -= novcanice[index];
            brojac++;
        }
        if (brojac > 0) {
            System.out.println(String.format("%d*%s", brojac, formatiraj(novcanice[index])));
        }
        index++;
        if (iznos > 0) {
            izvratiOstatak(iznos);
        }
    }

    public static double zaokruziCudniDouble(double value, int scale) {
        return Math.round(value * Math.pow(10, scale)) / Math.pow(10, scale);
    }
    
    //makni nule iza decimalne tocke
    public String formatiraj(double d) {
        if (d == (long) d) {
            return String.format("%d", (long) d);
        } else {
            return String.format("%s", d);
        }
    }

}
