## JAVA1
### Vjezbe1 - Zadatak2
Napišite program koji učitava broj koji predstavlja iznos u kunama koji prodavačica treba uzvratiti klijentu. (Demo klase Scanner) Prodavačica uvijek uzvraća u najvećim novčanicama (kovanicama). Program treba ispisati u koliko kojih novčanica (kovanica) treba uzvratiti. Novčanice u HR imaju vrijednost 1000, 500, 200, 100, 50, 20 i 10 kuna, a kovanice su od 5, 2, 1, 0.50, 0.20, 0.10, 0.05, 0.02 i 0.01 kuna. Npr. ako je unesena vrijednost 1978, program treba ispisati:

Morate uzvratiti:

```
#!javascript

1*1000
1*500
2*200
1*50
1*20
1*5
1*2
1*1
```